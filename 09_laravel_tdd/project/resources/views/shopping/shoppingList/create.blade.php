<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight bg-green-100 rounded-lg">
            {{ __('Adding product') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form action="{{route('shoppingList.product.store',$shoppingList)}}" method="post">
            @csrf
            <dl>
                <div class="bg-gray-50 px-2 py-8 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 overflow-hidden shadow-sm sm:rounded-lg ">
                    <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                        <div>
                            <x-label for="product" :value="__('PRODUCT')" />
                            <x-input id="product" class="block mt-1 w-full" type="text" name="product"  autofocus />
                        </div>
                    </dd>
                </div>
                <div class="border-t border-gray-200">
                    <div class="bg-gray-50 px-2 py-8 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 overflow-hidden shadow-sm sm:rounded-lg ">
                        <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                            <div>
                                <x-label for="frequency" :value="__('BUYING FREQUENCY [in days]')" />
                                <x-input id="frequency" class="block mt-1 w-full" type="text" name="frequency"  autofocus />
                            </div>
                        </dd>
                    </div>

                    <div class="bg-green-100 px-2 py-4 pb-5 flex items-center justify-end mt-4 rounded-lg">
                        <x-button class="ml-4">
                            {{ __('ADD PRODUCT') }}
                        </x-button>
                    </div>
                </div>
            </dl>
        </form>
    </div>
</x-app-layout>
