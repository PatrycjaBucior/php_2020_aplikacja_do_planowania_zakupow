<?php

namespace App\Http\Controllers\Shopping;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductFrequency;
use App\Models\ShoppingList;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProductController extends Controller
{
    public function store(Request $request, ShoppingList $shoppingList)
    {
        $this->validate($request, [
            'product' => 'required',
            'frequency' => 'numeric|nullable'
        ]);

        $frequency = ProductFrequency::where('user_id', Auth::id())->where('product', $request->product)->first();
        if (!$frequency) {
            $frequency = new ProductFrequency();
            $frequency->user_id = Auth::id();
        }
        if ($request->frequency != "") {
            $frequency->frequency = $request->frequency;
            $frequency->fr_manually_added = true;
        } else {
            $frequency->fr_manually_added = false;
            $frequency->frequency = 0;
        }
        $frequency->product = $request->product;
        $frequency->save();


        $product = Product::where('product', $request->product)->first();

        if (!$product) {
            $product = new Product();
            $product->product = $request->product;
            $product->save();
        }
        if ($shoppingList->products->contains($product) == false) {
            $shoppingList->products()->attach($product->id, ['shopping_list_id' => $shoppingList->id]);
        }
        return redirect()->route('shoppingList.index');
    }

    public function create(ShoppingList $shoppingList)
    {
        return view('shopping.shoppingList.create')->withShoppingList($shoppingList);
    }
}
