<?php


namespace App\Http\Controllers\Shopping;

use App\Models\Product;
use App\Models\ProductFrequency;
use App\Models\ShoppingList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShoppingListController extends Controller
{
    public function index()
    {
        $shoppingList = ShoppingList::where('user_id',Auth::id())->where('done',false)->latest()->first();
        $boughtItems = array();
        if($shoppingList) {
            $boughtItems = $shoppingList->products;
            $productFreq = Auth::user()->frequencies;
            $boughtItems = $boughtItems->map(function ($value, $key) use ($productFreq) {
                $collection = $productFreq->where('product', $value->product)->where('is_bought', true);
                return $collection;
            });
            $boughtItems = $boughtItems->flatten();
            $items = $shoppingList->products->count();
            if ($boughtItems->count() == $items && $items > 0) {
                $shoppingList->done = true;
                $shoppingList->save();
            }
        }

        return view("shopping.shoppingList.index",['boughtItems' => $boughtItems])->withShoppingList($shoppingList);
    }

    public function create()
    {
        $shoppingList = new ShoppingList();
        $shoppingList->user_id = Auth::id();
        $shoppingList->done = false;
        $productFreq = Auth::user()->frequencies;
        foreach( $productFreq as $product){
            $product->is_bought = false;
            $product->save();
        }
        $shoppingList->save();

        return redirect()->route('shoppingList.index');
    }

    public function update(ShoppingList $shoppingList, Request $request)
    {
        foreach ($shoppingList->products as $product)
        {
            $freq = ProductFrequency::where('user_id',Auth::id())->where('product',$product->product)->first();
            if ($request->input('checkbox'.$product->id) == 'on')
            {
                $freq->is_bought = true;
                $freq->save();
            }
        }
        return redirect()->route('shoppingList.index');
    }



}
