<?php

namespace App\Http\Controllers\Shopping;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductFrequency;
use App\Models\ShoppingList;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;

class GenerateController extends Controller
{
    public function index()
    {
        $shoppingList = $this->fetch_data();
        //return view('shopping.shoppingList.index')->withShoppingList($shoppingList);
        return redirect()->route('shoppingList.index');

    }

    public function calculate_frequency($item, $user_id)
    {
        $lists = ShoppingList::where('user_id', $user_id)->whereHas('products', function (Builder $query) use ($item) {
            $query->where('product', $item);
        })->get();
        $dates = array();
        foreach($lists as $list)
        {
            array_push($dates, $list->created_at);
        }
        if(count($dates) < 2)
        {
            return -1;
        }
        asort($dates);

        $diff = $dates[0];
        $avg = 0;
        for($i=1; $i<count($dates); $i++)
        {
            $avg += ($diff->diffInDays($dates[$i]));
            $diff = $dates[$i];
        }
        $avg /= (count($dates)-1);
        return $avg;
    }

    public function fetch_data()
    {
        $user_id = Auth::id(); //get the id of currently logged in user

        $product_frequencies = ProductFrequency::where('user_id', $user_id)->get();
        foreach($product_frequencies as $fr) {
            if ($fr->fr_manually_added == 0) {
                $new_frequency = $this->calculate_frequency($fr->product, $user_id);
                if($new_frequency != -1) {
                    ProductFrequency::where('user_id', $user_id)->where('product', $fr->product)->update(['frequency' => $new_frequency]);
                }
            }
        }
        return $this->generate_list($user_id);

    }

    public function generate_list($user_id)
    {
        $shopping_list = new ShoppingList();
        $shopping_list->user_id = $user_id;
        $shopping_list->done = false;
        $shopping_list->save();
        //function calculating what the user should currently buy, based on frequencies and current date

        $mytime = Carbon::now()->toDateTimeString(); //current time
        $products = ProductFrequency::where('user_id', $user_id)->get(); //products of current user

        foreach($products as $product) {
            $product_name = $product->product;

            // querying the created_time of the latest list which contains product checked in this loop iteration
                 $list = ShoppingList::where('user_id', $user_id)->whereHas('products', function (Builder $query) use ($product_name) {
                    $query->where('product', $product_name);
                })->latest()->get();
                 $list_count = $list->count();
                $product_fr_man = ProductFrequency::where('user_id', $user_id)->where('product', $product_name)->first();

                 if($list_count < 2 && $product_fr_man->fr_manually_added == 0)
                 {
                     continue;
                 }

            $date = $list[0]->created_at;
            if(($date->addDays($product->frequency)) <= $mytime)
            {
                $prod_freq = Auth::user()->frequencies->where('product', $product_name)->first();
                $prod_freq->is_bought = false;
                $prod_freq->save();
                $prod_to_connect = Product::where('product', $product_name)->first();
                $shopping_list->products()->attach($prod_to_connect->id,['shopping_list_id' => $shopping_list->id]);
            }
        }

        return $shopping_list;
    }
}
