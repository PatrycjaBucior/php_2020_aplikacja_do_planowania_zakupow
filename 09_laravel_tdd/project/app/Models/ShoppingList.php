<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    use HasFactory;
    public function products()
    {
        return $this->belongsToMany(Product::class,'list_products','shopping_list_id','product_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
