<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFrequency extends Model
{
    protected $fillable = ['frequency'];
    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
