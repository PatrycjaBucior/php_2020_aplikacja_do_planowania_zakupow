<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function login($email, $password)
    {
        $I = $this;
        $I->amOnPage('/login');
        $I->submitForm('#login_form',[
            'email' => $email,
            'password' => $password
        ]);
    }

    public function addProduct($listid,$product)
    {
        $I = $this;
        $I->amOnPage('/shoppingList/'.$listid.'/product/create');
        $I->fillField('product',$product);
        $I->click('ADD PRODUCT');
    }
}
