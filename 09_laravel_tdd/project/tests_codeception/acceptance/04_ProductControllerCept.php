<?php
$I = new AcceptanceTester($scenario ?? null);

$I->wantTo('have test adding products to shopping list page');

$I->amOnPage('/dashboard');

$I->seeCurrentUrlEquals('/login');

$I->fillField('email', 'john.doe@gmail.com');
$I->fillField('password', 'secret');

$I->click('Login');

$I->seeCurrentUrlEquals('/dashboard');

$I->amOnPage('/shoppingList');

$I->see('My shopping list', 'h2');
$I->see('You have no current shopping list! Create one!');

$I->click('CREATE MY LIST');

$I->seeCurrentUrlEquals('/shoppingList');

$I->click('ADD PRODUCT');

$product = "mleko";
$frequency = "3";
$fr_manually_added = true;
$is_bought = false;

$I->fillField('product', $product);
$I->fillField('frequency', 'string');

$I->click('ADD PRODUCT');

$I->fillField('product', $product);
$I->fillField('frequency', $frequency);

$I->dontSeeInDatabase('products', [
    'product' => $product,
]);

$I->dontSeeInDatabase('product_frequencies', [
    'frequency' => $frequency,
    'is_bought' => $is_bought
]);


$I->click('ADD PRODUCT');

$I->seeInDatabase('products', [
    'product' => $product,
]);

$I->seeInDatabase('product_frequencies', [
    'frequency' => $frequency,
    'fr_manually_added' => $fr_manually_added,
    'is_bought' => $is_bought
]);

$I->seeCurrentUrlEquals('/shoppingList');

$I->click('ADD PRODUCT');

$product = "jajka";
$frequency = "";
$fr_manually_added = false;
$is_bought = false;

$I->fillField('product', $product);
$I->fillField('frequency', 'string');

$I->click('ADD PRODUCT');

$I->fillField('product', $product);
$I->fillField('frequency', $frequency);

$I->dontSeeInDatabase('products', [
    'product' => $product,
]);

$I->dontSeeInDatabase('product_frequencies', [
    'frequency' => $frequency,
    'is_bought' => $is_bought
]);


$I->click('ADD PRODUCT');

$I->seeInDatabase('products', [
    'product' => $product,
]);

$I->seeInDatabase('product_frequencies', [
    'frequency' => 0,
    'fr_manually_added' => $fr_manually_added,
    'is_bought' => $is_bought
]);

$I->seeCurrentUrlEquals('/shoppingList');
