<?php

$I = new AcceptanceTester($scenario);

$I->wantTo('Test list generation, when file frequencies are not added manually');

$I->seeNumRecords(0, "shopping_lists");
$I->seeNumRecords(0, "products");
$I->seeNumRecords(0, "list_products");

$user_id = 1;
$done = true;
$created_at = '2021-01-03 15:18:28';
$product_name = 'mleko';
$is_bought = true;
$frequency = 0;
$fr_manually_added = false;

$created_at_2 = '2021-01-07 15:18:28';
$product_name_2 = 'kokos';
$is_bought_2 = true;
$frequency_2 = 0;
$fr_manually_added_2 = false;

$shopping_list_id = $I->haveInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at]);
$product_id = $I->haveInDatabase('products', ['product' => $product_name]);
$product_id_2 = $I->haveInDatabase('products', ['product' => $product_name_2]);
$list_product = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id]);
$list_product_2 = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id_2]);
$prod_frequency_id = $I->haveInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);
$prod_frequency_id_2 = $I->haveInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name_2, 'frequency' => $frequency_2, 'fr_manually_added' => $fr_manually_added_2, 'is_bought' => $is_bought_2]);

$I->seeInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at]);
$I->seeInDatabase('products', ['product' => $product_name]);
$I->seeInDatabase('products', ['product' => $product_name_2]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id_2]);
$I->seeInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);
$I->seeInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name_2, 'frequency' => $frequency_2, 'fr_manually_added' => $fr_manually_added_2, 'is_bought' => $is_bought_2]);

$shopping_list_id_2 = $I->haveInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at_2]);
$list_product_3 = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id_2, 'product_id' => $product_id]);
$list_product_4 = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id_2, 'product_id' => $product_id_2]);

$I->seeInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at_2]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id_2, 'product_id' => $product_id]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id_2, 'product_id' => $product_id_2]);

$I->amOnPage('/dashboard');
$I->seeCurrentUrlEquals('/login');

$I->fillField('email', 'john.doe@gmail.com');
$I->fillField('password', 'secret');

$I->click('Login');

$I->seeCurrentUrlEquals('/dashboard');

$I->click('My Shopping List');

$I->see('My shopping list', 'h2');
$I->see('Shopping list', 'h3');

$I->amOnPage('/shoppingList');
$I->see('GENERATE MY LIST');
$I->see('CREATE MY LIST');

$I->click('GENERATE MY LIST');

$I->see('mleko');
$I->see('kokos');

