<?php
$I = new AcceptanceTester($scenario);

$I->login('john.doe@gmail.com','secret');

$I->amOnPage('/shoppingList');
$I->seeCurrentUrlEquals('/shoppingList');

$I->seeNumRecords(0,'shopping_lists');
$I->seeNumRecords(0,'list_products');
$I->see('My shopping list','h2');
$I->see('You have no current shopping list! Create One!');
$I->dontSee('ADD PRODUCT');
$I->dontSee('UPDATE LIST');


$I->click('CREATE MY LIST');
$I->seeCurrentUrlEquals('/shoppingList');
$I->seeNumRecords(1,'shopping_lists');

$I->seeNumRecords(0,'list_products');
$I->dontSee('You have no current shopping list! Create one!');
$I->see('ADD PRODUCT');
$I->see('UPDATE LIST');

$userId = $I->grabFromDatabase('users','id',[
   'email' => 'john.doe@gmail.com'
]);
$listId = $I->grabFromDatabase('shopping_lists','id',[
    'user_id' => $userId
]);

$productName = 'mleko';
$productName2 = 'kawa';
$productName3 = 'masło';

$I->addProduct($listId,$productName);
$I->addProduct($listId,$productName2);
$I->addProduct($listId,$productName3);

$productId = $I->grabFromDatabase('products','id',[
   'product' => $productName
]);

$productId2 = $I->grabFromDatabase('products','id',[
    'product' => $productName2
]);

$productId3 = $I->grabFromDatabase('products','id',[
    'product' => $productName3
]);

$I->seeCurrentUrlEquals('/shoppingList');

$I->seeInDatabase('list_products',[
   'shopping_list_id' => $listId,
   'product_id' => $productId
]);
$I->seeInDatabase('list_products',[
    'shopping_list_id' => $listId,
    'product_id' => $productId2
]);
$I->seeInDatabase('list_products',[
    'shopping_list_id' => $listId,
    'product_id' => $productId3
]);
$I->see($productName);
$I->see($productName2);
$I->see($productName3);
$I->dontSee("$productName", "del");
$I->dontSee("$productName2", "del");
$I->dontSee("$productName3", "del");
$I->dontSeeCheckboxIsChecked("checkbox$productId");
$I->dontSeeCheckboxIsChecked("checkbox$productId2");
$I->dontSeeCheckboxIsChecked("checkbox$productId3");
$I->checkOption("checkbox$productId2");
$I->seeCheckboxIsChecked("checkbox$productId2");
$I->see('UPDATE LIST');
$I->see('ADD PRODUCT');

$I->seeInDatabase('product_frequencies',[
   'user_id' => $userId,
   'product' => $productName,
   'is_bought' => false
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName2,
    'is_bought' => false
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName3,
    'is_bought' => false
]);

$I->click('UPDATE LIST');
$I->seeInCurrentUrl('/shoppingList');


$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName,
    'is_bought' => false
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName2,
    'is_bought' => true
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName3,
    'is_bought' => false
]);

$I->seeInDatabase('shopping_lists',[
   'user_id' => $userId,
   'done' => false
]);

$I->see("$productName");
$I->see("$productName2", "del");
$I->see("$productName3");
$I->dontSee("$productName", "del");
$I->dontSee("$productName3", "del");
$I->see('UPDATE LIST');
$I->see('ADD PRODUCT');


$I->dontSeeCheckboxIsChecked("checkbox$productId");
$I->dontSeeCheckboxIsChecked("checkbox$productId3");
$I->checkOption("checkbox$productId");
$I->checkOption("checkbox$productId3");
$I->seeCheckboxIsChecked("checkbox$productId");
$I->seeCheckboxIsChecked("checkbox$productId3");

$I->click('UPDATE LIST');
$I->seeInCurrentUrl('/shoppingList');

$I->see('You have no current shopping list! Create One!');
$I->see('GENERATE MY LIST');
$I->see('CREATE MY LIST');
$I->dontSee('ADD PRODUCT');
$I->dontSee('UPDATE LIST');

$I->seeInDatabase('shopping_lists',[
   'user_id' => $userId,
   'done' => true
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName,
    'is_bought' => true
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName2,
    'is_bought' => true
]);

$I->seeInDatabase('product_frequencies',[
    'user_id' => $userId,
    'product' => $productName3,
    'is_bought' => true
]);

$I->seeInDatabase('list_products',[
    'shopping_list_id' => $listId,
    'product_id' => $productId
]);
$I->seeInDatabase('list_products',[
    'shopping_list_id' => $listId,
    'product_id' => $productId2
]);
$I->seeInDatabase('list_products',[
    'shopping_list_id' => $listId,
    'product_id' => $productId3
]);


