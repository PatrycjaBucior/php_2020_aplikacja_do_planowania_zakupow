<?php

$I = new AcceptanceTester($scenario ?? null);

$I->wantTo('test shopping history page');

$I->seeNumRecords(0, "shopping_lists");
$I->seeNumRecords(0, "products");
$I->seeNumRecords(0, "list_products");
$I->seeNumRecords(1, "users");

$user_name = 'User';
$email = 'email@example.com';
$password_hash = '$2y$10$aKlLz4QeO/CmJGiKxbcUHOQrSz3sD/V0Yi.cvmDbcfmNolHcTdm5y';
$user_id2 = $I->haveInDatabase('users', ['name' => $user_name, 'email' => $email, 'password' => $password_hash]);
$I->seeInDatabase('users', ['name' => $user_name, 'email' => $email, 'password' => $password_hash]);

$user_id = 1;
$done = true;
$created_at = '2021-01-21 15:18:28';
$created_at2 = '2020-01-21 15:18:28';
$product_name = 'mleko';
$product_name2 = 'chleb';
$is_bought = true;
$frequency = 2;
$fr_manually_added = true;

$shopping_list_id2 = $I->haveInDatabase('shopping_lists', ['user_id' => $user_id2, 'done' => $done, 'created_at' => $created_at2]);
$product_id2 = $I->haveInDatabase('products', ['product' => $product_name2]);
$list_product2 = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id2, 'product_id' => $product_id2]);
$prod_frequency_id2 = $I->haveInDatabase('product_frequencies', ['user_id' => $user_id2, 'product' => $product_name2, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);

$I->seeInDatabase('shopping_lists', ['user_id' => $user_id2, 'done' => $done, 'created_at' => $created_at2]);
$I->seeInDatabase('products', ['product' => $product_name2]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id2, 'product_id' => $product_id2]);
$I->seeInDatabase('product_frequencies', ['user_id' => $user_id2, 'product' => $product_name2, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);


$I->amOnPage('/history');
$I->seeCurrentUrlEquals('/login');

$I->fillField('email', 'john.doe@gmail.com');
$I->fillField('password', 'secret');

$I->click('Login');

$I->seeCurrentUrlEquals('/history');

$I->see('Shopping history', 'h2');
$I->see('You have no shopping lists.');

$shopping_list_id = $I->haveInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at]);
$product_id = $I->haveInDatabase('products', ['product' => $product_name]);
$list_product = $I->haveInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id]);
$prod_frequency_id = $I->haveInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);

$I->seeInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at]);
$I->seeInDatabase('products', ['product' => $product_name]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id]);
$I->seeInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);

$I->amOnPage('/dashboard');

$I->click('Shopping history');

$I->amOnPage('/history');

$I->see($created_at);
$I->dontSee($created_at2);

$I->click('Details');

$I->amOnPage('/history/' . $shopping_list_id);

$I->see('Viewing a shopping list created at ' . $created_at);
$I->see('Products');
$I->see($product_name, 'li');
$I->dontSee($product_name2, 'li');

$I->click('Delete');

$I->dontSeeInDatabase('shopping_lists', ['user_id' => $user_id, 'done' => $done, 'created_at' => $created_at]);
$I->dontSeeInDatabase('products', ['product' => $product_name]);
$I->dontSeeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id, 'product_id' => $product_id]);
$I->dontSeeInDatabase('product_frequencies', ['user_id' => $user_id, 'product' => $product_name, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);

$I->seeInDatabase('shopping_lists', ['user_id' => $user_id2, 'done' => $done, 'created_at' => $created_at2]);
$I->seeInDatabase('products', ['product' => $product_name2]);
$I->seeInDatabase('list_products', ['shopping_list_id' => $shopping_list_id2, 'product_id' => $product_id2]);
$I->seeInDatabase('product_frequencies', ['user_id' => $user_id2, 'product' => $product_name2, 'frequency' => $frequency, 'fr_manually_added' => $fr_manually_added, 'is_bought' => $is_bought]);


$I->amOnPage('/history');
$I->see('Shopping history', 'h2');
$I->see('You have no shopping lists.');
